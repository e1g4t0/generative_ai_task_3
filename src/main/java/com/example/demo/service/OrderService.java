package com.example.demo.service;

import com.example.demo.entity.Order;
import com.example.demo.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public Order getOrderById(Long id) {
        return orderRepository.findById(id).orElse(null);
    }

    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }

    public Order updateOrder(Long id, Order updatedOrder) {
        Order existingOrder = getOrderById(id);

        if (existingOrder == null) {
            return null; // Order not found
        }

        existingOrder.setUser(updatedOrder.getUser());
        existingOrder.setCartItems(updatedOrder.getCartItems());
        existingOrder.setTotalAmount(updatedOrder.getTotalAmount());

        return orderRepository.save(existingOrder);
    }

    public boolean deleteOrder(Long id) {
        Order existingOrder = getOrderById(id);

        if (existingOrder == null) {
            return false; // Order not found
        }

        orderRepository.delete(existingOrder);
        return true;
    }
}

