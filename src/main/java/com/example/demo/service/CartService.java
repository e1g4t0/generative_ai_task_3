package com.example.demo.service;

import com.example.demo.entity.CartItem;
import com.example.demo.repository.CartItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartService {
    @Autowired
    private CartItemRepository cartItemRepository;

    public List<CartItem> getAllCartItems() {
        return cartItemRepository.findAll();
    }

    public CartItem getCartItemById(Long id) {
        return cartItemRepository.findById(id).orElse(null);
    }

    public CartItem createCartItem(CartItem cartItem) {
        return cartItemRepository.save(cartItem);
    }

    public CartItem updateCartItem(Long id, CartItem updatedCartItem) {
        CartItem existingCartItem = getCartItemById(id);

        if (existingCartItem == null) {
            return null; // Cart item not found
        }

        existingCartItem.setUser(updatedCartItem.getUser());
        existingCartItem.setProduct(updatedCartItem.getProduct());
        existingCartItem.setQuantity(updatedCartItem.getQuantity());

        return cartItemRepository.save(existingCartItem);
    }

    public boolean deleteCartItem(Long id) {
        CartItem existingCartItem = getCartItemById(id);

        if (existingCartItem == null) {
            return false; // Cart item not found
        }

        cartItemRepository.delete(existingCartItem);
        return true;
    }
}
