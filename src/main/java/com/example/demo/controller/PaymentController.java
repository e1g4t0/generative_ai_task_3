package com.example.demo.controller;

import com.example.demo.service.PaymentService;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payments")
public class PaymentController {
    @Autowired
    private PaymentService paymentService;

    @PostMapping("/charge")
    public ResponseEntity<String> chargePayment(
            @RequestParam("amount") double amount,
            @RequestParam("currency") String currency,
            @RequestParam("token") String token
    ) {
        try {
            int amountInCents = (int)(amount * 100);
            Charge charge = paymentService.chargePayment(amountInCents, currency, token);
            // Handle successful payment, save to database, send confirmation email, etc.
            return ResponseEntity.ok("Payment Successful: " + charge.getId());
        } catch (StripeException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Payment Failed: " + e.getMessage());
        }
    }
}
